//
// -*- C++ -*-
//
// -----------------------------------------------------------------------
//                          HEP Random
//                       --- MixMaxRng ---
//                     class implementation file
// -----------------------------------------------------------------------
//
// This file interfaces the MixMax PseudoRandom Number Generator
// proposed by:
//
// G.K.Savvidy and N.G.Ter-Arutyunian,
//   On the Monte Carlo simulation of physical systems,
//   J.Comput.Phys. 97, 566 (1991);
//   Preprint EPI-865-16-86, Yerevan, Jan. 1986
//   http://dx.doi.org/10.1016/0021-9991(91)90015-D
//
// K.Savvidy
//   "The MIXMAX random number generator"
//   Comp. Phys. Commun. (2015)
//   http://dx.doi.org/10.1016/j.cpc.2015.06.003
//
// K.Savvidy and G.Savvidy
//   "Spectrum and Entropy of C-systems. MIXMAX random number generator"
//   Chaos, Solitons & Fractals, Volume 91, (2016) pp. 33-38
//   http://dx.doi.org/10.1016/j.chaos.2016.05.003
//
// Original implementation by Konstantin Savvidy - Copyright 2004-2017
// Updated by Marco Barbone -- 2023

#include "CLHEP/Random/MixMaxRng.h"
#include "CLHEP/Random/engineIDulong.h"
#include "CLHEP/Utility/atomic_int.h"

#include <atomic>
#include <iostream>
#include <vector>

const unsigned long MASK32=0xffffffff;

namespace CLHEP {

  // Number of instances with automatic seed selection
    static CLHEP_ATOMIC_INT_TYPE numberOfEngines(0);

    MixMaxRng::MixMaxRng() : HepRandomEngine(), MixMaxEngine(++numberOfEngines){}

    MixMaxRng::MixMaxRng(uint64_t seed) : HepRandomEngine(), MixMaxEngine(seed) {
        if(seed == 0) {
            throw std::invalid_argument("MixMaxRng::MixMaxRng(uint64_t seed): seed cannot be 0");
        }
    }

    MixMaxRng::MixMaxRng(uint32_t clusterId, uint32_t machineId, uint32_t runId, uint32_t streamId) : HepRandomEngine(), MixMaxEngine(
            clusterId, machineId, runId, streamId) {}

    MixMaxRng::MixMaxRng(uint64_t seed, uint64_t stream) : HepRandomEngine(), MixMaxEngine(seed, stream) {}

    MixMaxRng::MixMaxRng(const uint64_t *state, uint64_t sumOverNew, uint32_t counter) : HepRandomEngine(), MixMaxEngine(state, sumOverNew,
                                                                                                      counter) {}

    MixMaxRng::MixMaxRng(std::istream is) : HepRandomEngine() {
        readState(is);
    }

    MixMaxRng::MixMaxRng(const MIXMAX::MixMaxRng17 &other) : HepRandomEngine(), MixMaxEngine(other) {}

    MixMaxRng::MixMaxRng(MIXMAX::MixMaxRng17 &&other) : HepRandomEngine(), MixMaxEngine(other) {}

    double MixMaxRng::flat() {
        return Uniform();
    }

    void MixMaxRng::flatArray(const int size, double *vect) {
        for (int i = 0; i < size; ++i) {
            vect[i] = Uniform();
        }
    }

    void MixMaxRng::setSeed(long seed, int) {
        seedLCG(seed);
    }

    void MixMaxRng::setSeeds(const long *seeds, int i) {
        if(i <= 0) {
            throw std::invalid_argument("MixMaxRng::setSeeds: i must be > 0");
        }

        std::uint32_t seed0 = seeds[0], seed1 = 0, seed2 = 0, seed3 = 0;

        if (i > 1) {
            seed1 = seeds[1];
        }
        if (i > 2) {
            seed2 = seeds[2];
        }
        if (i > 3) {
            seed3 = seeds[3];
        }
        applyBigSkip(seed3, seed2, seed1, seed0);
    }

    static constexpr auto file_header = "MixMaxRng State, file version 1.1";

    void MixMaxRng::saveStatus(const char *filename) const {
        std::ofstream outFile(filename, std::ios::out);
        if (!outFile.good()) {
            throw std::runtime_error("MixMaxRng::saveStatus(): cannot open file " + std::string(filename));
        }
        outFile << file_header << std::endl;
        printState(outFile);
        outFile << std::endl;
        outFile.close();
    }

    void MixMaxRng::restoreStatus(const char *filename) {
        std::ifstream inFile(filename, std::ios::in);
        if (!inFile.good()) {
            throw std::runtime_error("MixMaxRng::restoreStatus(): cannot open file " + std::string(filename));
        }
        std::string line;
        std::getline(inFile, line);
        if (line != file_header) {
            throw std::runtime_error("MixMaxRng::restoreStatus(): wrong engine type " + line);
        }
        std::getline(inFile, line);
        std::istringstream iss(line);
        readState(iss);
    }

    void MixMaxRng::showStatus() const {
        std::cout << "----- MixMaxRng engine status -----" << std::endl;
        printState(std::cout);
        std::cout << std::endl;
        std::cout << "------------------------------------" << std::endl;
    }

    std::string MixMaxRng::name() const {
        return "MixMaxRng";
    }

    std::string MixMaxRng::engineName() {
        return "MixMaxRng";
    }

    std::ostream& MixMaxRng::put(std::ostream& os) const {
        auto precision = os.precision(20);
        os << " ";
        os << beginTag() << " ";
        printState(os);
        os << endTag() << std::endl;
        os.precision(precision);
        return os;
    }

    std::istream& MixMaxRng::getState( std::istream& is ) {
        readState(is);
        std::string tag;
        is >> tag;
        if (tag != endTag()) {
            throw std::runtime_error("MixMaxRng::get: wrong end tag while reading state");
        }
        return is;
    }

    std::istream& MixMaxRng::get(std::istream& is) {
        std::string tag;
        is >> tag;
        if (tag != beginTag()) {
            throw std::runtime_error("MixMaxRng::get: wrong begin tag while reading state");
        }
        return getState(is);
    }


    std::vector<unsigned long> MixMaxRng::put () const {
        // The code below assumes that unsigned long is at least 64 bits
        static_assert(sizeof(unsigned long) >= sizeof(uint64_t), "sizeof(unsigned long) >= sizeof(uint64_t)");
        std::vector<unsigned long> v;
        v.push_back(engineIDulong<MixMaxRng>());
        // push the state in the vector
        for (int i = 0; i <  MIXMAX::MixMaxRng17::N; ++i) {
            v.push_back(m_State[i]);
        }
        v.push_back(m_SumOverNew);
        v.push_back(m_Counter);
        return v;
    }

    constexpr unsigned int MixMaxRng::VECTOR_STATE_SIZE() {
        return 1 + MIXMAX::MixMaxRng17::N + 1 + 1;
    }

    bool MixMaxRng::getState(const std::vector<unsigned long> &v) {
        if(v.size() != VECTOR_STATE_SIZE()) {
            std::cerr <<"MixMaxRng::getState(): vector has wrong length - state unchanged\n";
            return false;
        }
        for (int i = 0; i < N; ++i) {
            m_State[i] = v[i + 1];
        }
        m_SumOverNew = v[MIXMAX::MixMaxRng17::N + 1];
        m_Counter = v[MIXMAX::MixMaxRng17::N + 2];
        return true;
    }

    bool MixMaxRng::get(const std::vector<unsigned long> &v) {
        if(v.size() != VECTOR_STATE_SIZE()) {
            std::cerr <<"MixMaxRng::getState(): vector has wrong length - state unchanged\n";
            return false;
        }

        const auto checksum = v[0];
        if(checksum != engineIDulong<MixMaxRng>()) {
            std::cerr <<"MixMaxRng::getState(): vector has wrong engine id - state unchanged\n";
            return false;
        }
        return getState(v);
    }

    std::ostream& MixMaxRng::printState(std::ostream &os) const {
        auto precision = os.precision(20);
        for (int i = 0; i < MIXMAX::MixMaxRng17::N; ++i) {
            os << m_State[i] << ' ';
        }
        os << m_SumOverNew << ' ';
        os << static_cast<std::uint64_t>(m_Counter) << ' ';
        os.precision(precision);
        return os;
    }

    std::istream& MixMaxRng::readState(std::istream &is) {
        for (int i = 0; i <  MIXMAX::MixMaxRng17::N; ++i) {
            is >> m_State[i];
        }
        std::uint64_t counter;
        is >> m_SumOverNew >> counter;
        m_Counter = counter;
        return is;
    }


}  // namespace CLHEP
